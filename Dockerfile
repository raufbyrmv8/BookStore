FROM openjdk
RUN echo "hello world"
COPY ./build/libs/BookStoreAgain-0.1.jar /app/
WORKDIR /app/
ENTRYPOINT ["java"]
CMD ["-jar", "/app/BookStoreAgain-0.1.jar"]