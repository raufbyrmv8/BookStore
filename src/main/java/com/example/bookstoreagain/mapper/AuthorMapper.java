package com.example.bookstoreagain.mapper;


import com.example.bookstoreagain.dto.AuthorDTO;
import com.example.bookstoreagain.model.Author;
import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;
import org.springframework.stereotype.Component;

import static org.mapstruct.ReportingPolicy.IGNORE;

@Mapper(componentModel = "spring",unmappedTargetPolicy = IGNORE)
@Component
public interface AuthorMapper {

    AuthorMapper INSTANCE = Mappers.getMapper(AuthorMapper.class);
    AuthorDTO authorToAuthorDto(Author author);
    @InheritInverseConfiguration
    Author authorDtoToAuthor(AuthorDTO authorDTO);
}
