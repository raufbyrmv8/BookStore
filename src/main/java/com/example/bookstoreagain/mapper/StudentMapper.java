package com.example.bookstoreagain.mapper;


import com.example.bookstoreagain.dto.StudentDTO;
import com.example.bookstoreagain.model.Student;
import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;
import org.springframework.stereotype.Component;

import static org.mapstruct.ReportingPolicy.IGNORE;

@Mapper(componentModel = "spring",unmappedTargetPolicy = IGNORE)
@Component
public interface StudentMapper {

    StudentMapper INSTANCE = Mappers.getMapper(StudentMapper.class);
    StudentDTO studentToStudentDto(Student student);
    @InheritInverseConfiguration
    Student studentDtoToStudent(StudentDTO studentDTO);
}
