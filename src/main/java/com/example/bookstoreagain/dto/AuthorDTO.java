package com.example.bookstoreagain.dto;

import lombok.*;
import lombok.experimental.FieldDefaults;

import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class AuthorDTO {
    String nameOfAuthor;
    int ageOfAuthor;
    List<BookDTO>bookList;
}
