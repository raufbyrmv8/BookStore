package com.example.bookstoreagain;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BookStoreAgainApplication {

    public static void main(String[] args) {
        SpringApplication.run(BookStoreAgainApplication.class, args);
    }

}
