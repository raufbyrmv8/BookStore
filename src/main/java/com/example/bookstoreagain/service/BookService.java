package com.example.bookstoreagain.service;



import com.example.bookstoreagain.dto.StudentDTO;

import java.util.List;

public interface BookService {
    List<StudentDTO> getStudentsForReadingBook(long bookId);
}
