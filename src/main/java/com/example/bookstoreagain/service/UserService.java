package com.example.bookstoreagain.service;


import com.example.bookstoreagain.security.User;

public interface UserService {
    User save(User user);
}
