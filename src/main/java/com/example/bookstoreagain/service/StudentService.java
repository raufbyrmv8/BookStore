package com.example.bookstoreagain.service;



import com.example.bookstoreagain.dto.BookDTO;
import com.example.bookstoreagain.dto.StudentDTO;

import java.util.List;

public interface StudentService {
    StudentDTO save(StudentDTO studentDTO);

    List<BookDTO> getReadingBooksForStudents(long id);
}
