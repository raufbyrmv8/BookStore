package com.example.bookstoreagain.service;


import com.example.bookstoreagain.dto.AuthorDTO;

public interface AuthorService {
    AuthorDTO save(AuthorDTO authorDTO);
    AuthorDTO deleteById(long id);
}
