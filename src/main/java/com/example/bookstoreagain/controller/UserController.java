package com.example.bookstoreagain.controller;


import com.example.bookstoreagain.security.User;
import com.example.bookstoreagain.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RequiredArgsConstructor
@RestController
@RequestMapping("/security")
public class UserController {
    private final UserService userService;
    @PostMapping("/save")
    public User save(@RequestBody User user){
        return userService.save(user);
    }
}
