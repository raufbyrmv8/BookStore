package com.example.bookstoreagain.controller;


import com.example.bookstoreagain.dto.BookDTO;
import com.example.bookstoreagain.dto.StudentDTO;
import com.example.bookstoreagain.service.StudentService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/student")
@RequiredArgsConstructor
public class StudentController {
    private final StudentService studentService;
    @PostMapping("/save")
    public StudentDTO save(@RequestBody StudentDTO studentDTO){
        return studentService.save(studentDTO);
    }
    @GetMapping("/{studentId}/reading-books")
    public List<BookDTO>getReadingBooksForStudent(@PathVariable long studentId){
        return studentService.getReadingBooksForStudents(studentId);
    }
}
