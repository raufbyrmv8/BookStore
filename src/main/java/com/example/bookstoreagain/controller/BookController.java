package com.example.bookstoreagain.controller;


import com.example.bookstoreagain.dto.StudentDTO;
import com.example.bookstoreagain.service.BookService;
import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
@RestController
@RequestMapping("/book")
@RequiredArgsConstructor
public class BookController {
    private final BookService bookService;
    @GetMapping("/{bookId}/students")
    @Transactional
    public List<StudentDTO> getStudentsForReadingBooks(@PathVariable long bookId){
        return bookService.getStudentsForReadingBook(bookId);
    }
}
