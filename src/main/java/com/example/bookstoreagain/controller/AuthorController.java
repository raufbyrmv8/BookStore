package com.example.bookstoreagain.controller;


import com.example.bookstoreagain.dto.AuthorDTO;
import com.example.bookstoreagain.service.AuthorService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.repository.query.Param;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/author")
@RequiredArgsConstructor
public class AuthorController {
    private final AuthorService authorService;
    @PostMapping("/save")
    public AuthorDTO save(@RequestBody AuthorDTO authorDTO){
        return authorService.save(authorDTO);
    }
    @DeleteMapping("/delete")
    public AuthorDTO deleteById(@Param("id") long id){
        return authorService.deleteById(id);
    }
}
