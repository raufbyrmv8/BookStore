package com.example.bookstoreagain.repository;


import com.example.bookstoreagain.model.Author;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface AuthorRepository extends JpaRepository<Author,Long> {
    @EntityGraph(value = "Author.bookList")
    Optional<Author>findById(long id);
}
