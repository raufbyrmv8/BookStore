package com.example.bookstoreagain.repository;


import com.example.bookstoreagain.model.Student;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface StudentRepository extends JpaRepository<Student,Long> {
    List<Student> findByReadingBooksId(long bookId);
}
