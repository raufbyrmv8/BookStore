package com.example.bookstoreagain.model;

import jakarta.persistence.*;
import lombok.*;
import lombok.experimental.FieldDefaults;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "author")
@Entity
@FieldDefaults(level = AccessLevel.PRIVATE)
@Builder
@ToString
@NamedEntityGraph(
        name = "Author.bookList",
        attributeNodes = @NamedAttributeNode("bookList")
)
public class Author {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    long id;
    String nameOfAuthor;
    int ageOfAuthor;
    @OneToMany(cascade = CascadeType.ALL,mappedBy = "author",fetch = FetchType.LAZY)
    List<Book> bookList;
}
