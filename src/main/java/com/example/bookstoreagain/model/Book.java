package com.example.bookstoreagain.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import jakarta.persistence.*;
import lombok.*;
import lombok.experimental.FieldDefaults;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "author_and_student_book")
@Entity
@FieldDefaults(level = AccessLevel.PRIVATE)
@Builder
@NamedEntityGraph(
        name = "Book.nameOfBook",
        attributeNodes = @NamedAttributeNode("nameOfBook")
)
public class Book {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    long id;
    String nameOfBook;
    @ManyToOne
    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    @JsonBackReference
    @JoinColumn(name = "author_id",referencedColumnName = "id")
    Author author;


    @ManyToOne
    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    @JsonBackReference
    @JoinColumn(name = "student_id",referencedColumnName = "id")
    Student student;
}
