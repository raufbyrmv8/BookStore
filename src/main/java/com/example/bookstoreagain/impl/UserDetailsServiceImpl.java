package com.example.bookstoreagain.impl;


import com.example.bookstoreagain.repository.UserRepository;
import com.example.bookstoreagain.security.User;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class UserDetailsServiceImpl implements UserDetailsService {

    private final UserRepository userRepository;
    private final BCryptPasswordEncoder passwordEncoder;
    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
        User userDb = userRepository.findByEmail(email).orElseThrow(()-> new RuntimeException("Not found!"));
        UserDetails userDetails = org.springframework.security.core.userdetails.User
                .builder()
                .username(userDb.getEmail())
                .password(passwordEncoder.encode(userDb.getPassword()))
                .authorities(userDb.getAuthorities().stream()
                        .map(authority -> new SimpleGrantedAuthority(authority.getType()))
                        .collect(Collectors.toSet()))
                .build();
        return userDetails;
    }
}
