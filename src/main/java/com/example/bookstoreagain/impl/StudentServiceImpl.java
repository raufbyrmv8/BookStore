package com.example.bookstoreagain.impl;


import com.example.bookstoreagain.dto.BookDTO;
import com.example.bookstoreagain.dto.StudentDTO;
import com.example.bookstoreagain.mapper.StudentMapper;
import com.example.bookstoreagain.model.Book;
import com.example.bookstoreagain.model.Student;
import com.example.bookstoreagain.repository.BookRepository;
import com.example.bookstoreagain.repository.StudentRepository;
import com.example.bookstoreagain.service.StudentService;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class StudentServiceImpl implements StudentService {
    private final StudentRepository studentRepository;
    private final BookRepository bookRepository;
    private final ModelMapper modelMapper;
    private final StudentMapper studentMapper;

    @Override
    public StudentDTO save(StudentDTO studentDTO) {
        Student student = studentMapper.INSTANCE.studentDtoToStudent(studentDTO);
        student.getReadingBooks()
                .forEach(book -> book.setStudent(student));
        List<Book> bookList = student.getReadingBooks();
        student.setReadingBooks(bookList);
        return studentMapper.studentToStudentDto(studentRepository.save(student));
    }

    @Override
    public List<BookDTO> getReadingBooksForStudents(long studentId) {
        Optional<Student>student = studentRepository.findById(studentId);
        if (student.isPresent()){
            return bookRepository.findByStudentId(studentId)
                    .stream()
                    .map(book -> modelMapper.map(book,BookDTO.class))
                    .collect(Collectors.toList());
        }else {
           return Collections.emptyList();
        }

    }
}