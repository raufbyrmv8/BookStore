package com.example.bookstoreagain.impl;


import com.example.bookstoreagain.dto.StudentDTO;
import com.example.bookstoreagain.model.Book;
import com.example.bookstoreagain.repository.BookRepository;
import com.example.bookstoreagain.repository.StudentRepository;
import com.example.bookstoreagain.service.BookService;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class BookServiceImpl implements BookService {
    private final BookRepository bookRepository;
    private final StudentRepository studentRepository;
    private final ModelMapper modelMapper;

    @Override
    public List<StudentDTO> getStudentsForReadingBook(long bookId) {
       Optional<Book>optionalBook = bookRepository.findById(bookId);
        if (optionalBook.isPresent()){
            return studentRepository.findByReadingBooksId(bookId)
                    .stream()
                    .map(student -> modelMapper.map(student,StudentDTO.class))
                    .collect(Collectors.toList());
        }else {
            return Collections.emptyList();
        }
    }
}
