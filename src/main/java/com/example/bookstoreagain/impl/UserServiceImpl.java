package com.example.bookstoreagain.impl;


import com.example.bookstoreagain.repository.UserRepository;
import com.example.bookstoreagain.security.Authority;
import com.example.bookstoreagain.security.User;
import com.example.bookstoreagain.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {
    private final UserRepository userRepository;
    @Override
    public User save(User user) {
        user.getAuthorities()
                .forEach(user1 -> user1.setUser(user));
        List<Authority>authorities = user.getAuthorities();
        user.setAuthorities(authorities);
        return userRepository.save(user);
    }
}
