package com.example.bookstoreagain.impl;


import com.example.bookstoreagain.dto.AuthorDTO;
import com.example.bookstoreagain.mapper.AuthorMapper;
import com.example.bookstoreagain.model.Author;
import com.example.bookstoreagain.model.Book;
import com.example.bookstoreagain.repository.AuthorRepository;
import com.example.bookstoreagain.service.AuthorService;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class AuthorServiceImpl implements AuthorService {
    private final AuthorRepository authorRepository;
    private final AuthorMapper authorMapper;
    private final ModelMapper modelMapper;
    @Override
    public AuthorDTO save(AuthorDTO authorDTO) {
        Author author = authorMapper.INSTANCE.authorDtoToAuthor(authorDTO);
        author.getBookList()
                .forEach(book -> book.setAuthor(author));
        List<Book>bookList =author.getBookList();
        author.setBookList(bookList);
        return authorMapper.authorToAuthorDto(authorRepository.save(author));
    }

    @Override
    public AuthorDTO deleteById(long id) {
        AuthorDTO authorDTO = authorRepository.findById(id)
                .map(author -> modelMapper.map(author,AuthorDTO.class))
                .orElseThrow(()-> new RuntimeException("not found!"));
        authorRepository.delete(modelMapper.map(authorDTO, Author.class));
        return authorDTO;
    }
}
